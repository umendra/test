const express = require("express");
const path = require("path");
const bodyparser = require("body-parser");
const dotenv = require("dotenv");
const mongoose = require("mongoose");
const app = express();
dotenv.config({path:".env"});
const connectDB = require("./Server/database/connection");


const PORT = process.env.PORT || 8080;

connectDB();

const userCountSchema = new mongoose.Schema({
  totalUsersVisited: { type: Number, default: 0 }
});

const UserCount = mongoose.model('UserCount', userCountSchema);


app.use(bodyparser.urlencoded({ extended: true }));

app.use(bodyparser.json());

app.set("view engine", "ejs");

app.use("/css", express.static(path.resolve(__dirname, "Assets/css")));
app.use("/img", express.static(path.resolve(__dirname, "Assets/img")));
app.use("/js", express.static(path.resolve(__dirname, "Assets/js")));

app.use("/", require("./Server/routes/router"));

var server = app.listen(PORT, () => {
  console.log(`Server is running on http://localhost:${PORT}`);
});


//#################################

app.get('/userCount', (req, res) => {
  res.json({ uc });
});

app.get('/totaluserCount', async (req, res) => {
  const result = await UserCount.findOne();
  let tuc=0;
      if (result) {
        const { totalUsersVisited } = result;
        console.log('Total users visited:', totalUsersVisited);
      tuc=totalUsersVisited;
      } else {
        console.log('User count document not found');
      }

  res.json({ tuc });
});

//#################################


const io = require("socket.io")(server, {
  allowEIO3: true, //False by default
});

var userConnection = [];
let uc=0;

io.on("connection", async (socket) => {
  console.log("Socket id is: ", socket.id);

  socket.on("userconnect",  async (data) => {
    console.log("Logged in username", data.displayName);
    userConnection.push({
      connectionId: socket.id,
      user_id: data.displayName,
    });

    var userCount = userConnection.length;
    console.log("UserCount", userCount);
    uc=userCount;


    
    try {

      UserCount.findOne({})
  .then(doc => {
    if (!doc) {
      const initialCount = new UserCount({ totalUsersVisited: 0 });
      return initialCount.save();
    }
    return doc;
  })
  .then((doc) => {
    console.log('User count document initialized in MongoDB');
  })
  .catch(err => console.error('Error initializing user count document:', err));

      // Increment the total users visited count in MongoDB
      await UserCount.findOneAndUpdate({}, { $inc: { totalUsersVisited: 1 } });
      
      // Fetch the updated user count
      const result = await UserCount.findOne();
      if (result) {
        const { totalUsersVisited } = result;
        console.log('Total users visited:', totalUsersVisited);
      } else {
        console.log('User count document not found');
      }
      


    } catch (error) {
      console.error('Error updating user count:', error);
    }


  });
  socket.on("offerSentToRemote", (data) => {
    var offerReceiver = userConnection.find(
      (o) => o.user_id === data.remoteUser
    );
    if (offerReceiver) {
      console.log("OfferReceiver user is: ", offerReceiver.connectionId);
      socket.to(offerReceiver.connectionId).emit("ReceiveOffer", data);
    }
  });
  socket.on("answerSentToUser1", (data) => {
    var answerReceiver = userConnection.find(
      (o) => o.user_id === data.receiver
    );
    if (answerReceiver) {
      console.log("answerReceiver user is: ", answerReceiver.connectionId);
      socket.to(answerReceiver.connectionId).emit("ReceiveAnswer", data);
    }
  });
  socket.on("candidateSentToUser", (data) => {
    var candidateReceiver = userConnection.find(
      (o) => o.user_id === data.remoteUser
    );
    if (candidateReceiver) {
      console.log(
        "candidateReceiver user is: ",
        candidateReceiver.connectionId
      );
      socket.to(candidateReceiver.connectionId).emit("candidateReceiver", data);
    }
  });

  socket.on("disconnect", () => {
    console.log("User disconnected");
    // var disUser = userConnection.find((p) => (p.connectionId = socket.id));
    // if (disUser) {
    userConnection = userConnection.filter((p) => p.connectionId !== socket.id);
    console.log(
      "Rest users username are: ",
      userConnection.map(function (user) {
        return user.user_id;
      })
    );
    // }
  });
  socket.on("remoteUserClosed", (data) => {
    var closedUser = userConnection.find((o) => o.user_id === data.remoteUser);
    if (closedUser) {
      console.log("closedUser user is: ", closedUser.connectionId);
      socket.to(closedUser.connectionId).emit("closedRemoteUser", data);
    }
  });
});
